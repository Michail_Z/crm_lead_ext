# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class crm_lead_ext(models.Model):
    _inherit = ['crm.lead']

    email_from = fields.Char('Email', help="Email address of the contact",
                             index=True, track_visibility="onchange")
